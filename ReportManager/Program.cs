﻿using ReportManager.ServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportManager
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceReference.ReportManagerClient rmc = new ServiceReference.ReportManagerClient();
            StringBuilder options = new StringBuilder();
            options.AppendLine("1. Get all alarms within the time interval");
            options.AppendLine("2. Get all alarms with priority #");
            options.AppendLine("3. Get all tag values within the time interval");
            options.AppendLine("4. Newest values of all analogue tags");
            options.AppendLine("5. Newest values of all digital tags");
            options.AppendLine("6. All values of # tag");
            options.Append(">> ");
            
            while (true)
            {
                Console.Write(options.ToString());
                string op = Console.ReadLine();

                switch (op)
                {
                    case "1":
                        DateTime from_1 = InputTimeStamp("Input lower timestamp boundary with format ");
                        DateTime to1 = InputTimeStamp("Input top timestamp boundary with format ");
                        DisplayAlarmEvents(rmc.GetAlarmsFromInterval(from_1, to1));
                        break;
                    case "2":
                        int priority = InputPriority();
                        DisplayAlarmEvents(rmc.GetAlarmsWithPriority(priority));
                        break;
                    case "3":
                        DateTime from_3 = InputTimeStamp("Input lower timestamp boundary with format ");
                        DateTime to3 = InputTimeStamp("Input top timestamp boundary with format ");
                        DisplayObservedTags(rmc.TagValuesFromInterval(from_3, to3));
                        break;
                    case "4":
                        DisplayObservedTags(rmc.NewestAITagValues());
                        break;
                    case "5":
                        DisplayObservedTags(rmc.NewestDITagValues()); 
                        break;
                    case "6":
                        int[] tags = rmc.GetTags();
                        int tagName = InputTagName(tags);
                        DisplayObservedTags(rmc.AllTagValues(tagName));
                        break;
                    default: continue;
                }
            }
        }

        public static DateTime InputTimeStamp(string flavorText)
        {
            string format = "yyyy-MM-dd HH:mm:ss";
            Console.Write(flavorText + "(" + format + "): ");
            string input = Console.ReadLine();
            DateTime timeStamp = DateTime.ParseExact(input, format, System.Globalization.CultureInfo.InvariantCulture);
            return timeStamp;
        }
    
        public static int InputPriority()
        {
            Console.Write("Input priority (1, 2, 3): ");
            return int.Parse(Console.ReadLine());
        }

        public static int InputTagName(int[] tags)
        {
            List<int> names = new List<int>(tags.Count());
            foreach (int i in tags)
            {
                Console.WriteLine(i);
                names.Add(i);
            }
            int retval = -1;
            while (!names.Contains(retval))
            {
                retval = int.Parse(Console.ReadLine());
            }
            return retval;
        }
    
        public static void DisplayObservedTags(ObservedTagData[] tags)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Name\tValue\tTimestamp\t\tType");
            sb.AppendLine("====\t=====\t=========\t\t====");
            foreach (var otb in tags)
            {
                sb.AppendLine(otb.TagName+"\t"+otb.Value+"\t"+otb.TimeStamp+"\t\t"+otb.TagType);
            }
            Console.WriteLine(sb.ToString());
        }
    
        public static void DisplayAlarmEvents(AlarmEvent[] alarms)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Priority\tTimestamp\t\tMessage");
            sb.AppendLine("========\t=========\t\t=======");
            foreach (var a in alarms)
            {
                sb.AppendLine(a.Priority + "\t" + a.TimeStamp + "\t\t" + a.Message);
            }
            Console.WriteLine(sb.ToString());
        }
    }
}
