﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace TrendingApp
{
    class TrendingAppCallback : ServiceReference.ITrendingAppCallback
    {
        public void TagValueChanged(string message)
        {
            Console.WriteLine(string.Concat(Enumerable.Repeat("=", message.Length)));
            Console.WriteLine(message);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            TrendingAppCallback tacb = new TrendingAppCallback();
            InstanceContext ic = new InstanceContext(tacb);
            ServiceReference.TrendingAppClient tac = new ServiceReference.TrendingAppClient(ic);
            tac.ConnectToService();
            while (true) { }
        }

    }
}
