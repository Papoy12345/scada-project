﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCADA_Core.Interfaces
{
    interface IAnalogue
    {
        int LowLimit { get; set; }
        int HighLimit { get; set; }
        string Units { get; set; }
    }
}
