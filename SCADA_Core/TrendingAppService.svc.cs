﻿using SCADA_Core.Model;
using SCADA_Core.Model.IOTags;
using SCADA_Core.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SCADA_Core
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TrendingAppService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select TrendingAppService.svc or TrendingAppService.svc.cs at the Solution Explorer and start debugging.
    public class TrendingAppService : ITrendingApp
    {
        private static readonly object proxyLock = new object();
        private static readonly List<ITrendingAppCallback> Proxies = new List<ITrendingAppCallback>();
        public ITrendingAppCallback TrendingAppProxy
        {
            get
            {
                return OperationContext.Current.GetCallbackChannel<ITrendingAppCallback>();
            }
        }
        public void ConnectToService()
        {
            Proxies.Add(TrendingAppProxy);
        }

        public static void SendToTrendingApp(Output tag, double oldValue)
        {
            lock (proxyLock)
            {
                foreach (ITrendingAppCallback p in Proxies)
                {
                    p.TagValueChanged(tag.ToStringWhenValueChanged(oldValue));
                }
            }
            
        }
    }
}
