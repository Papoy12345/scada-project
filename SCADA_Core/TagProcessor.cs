﻿using SCADA_Core.Model;
using SCADA_Core.Model.IOTags;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.ServiceModel;
using System.Threading;

namespace SCADA_Core
{
    public delegate void TagValueChanged(Input tag, double newValue);
    public delegate void TagCollectionChanged();
    public class TagProcessor
    {

        public static readonly object analogueInputTagsLock  = new object();
        public static readonly object analogueOutputTagsLock = new object();
        public static readonly object digitalInputTagsLock   = new object();
        public static readonly object digitalOutputTagsLock  = new object();
        public static readonly object alarmsLock             = new object();
        public static readonly object threadsLock            = new object();

        public static Dictionary<int, AnalogueInput >  analogueInputs;
        public static Dictionary<int, DigitalInput  >   digitalInputs;
        public static Dictionary<int, AnalogueOutput> analogueOutputs;
        public static Dictionary<int, DigitalOutput >  digitalOutputs;
        public static Dictionary<int, List<Alarm>> alarms;

        public static Dictionary<int, Thread> tagThreads = new Dictionary<int, Thread>();

        public static event TagValueChanged OnTagValueChanged;

        

        public static void InitializeTagCollections()
        {
            analogueInputs  = new Dictionary<int, AnalogueInput >   ();
            analogueOutputs = new Dictionary<int, AnalogueOutput>   ();
            digitalInputs   = new Dictionary<int, DigitalInput  >   ();
            digitalOutputs  = new Dictionary<int, DigitalOutput >   ();
        }

        public static void InitializeDelegates()
        {
            OnTagValueChanged += UpdateTagValueAtRuntime;
            OnTagValueChanged += HandleAlarms;

            DatabaseManagerService.OnTagCollectionChanged += UpdateTagXmlConfig;
        }
        

        public static void StartThreads()
        {
            AddAnalogueInputs();
            AddDigitalInputs();
            Start();
        }
        private static void AddAnalogueInputs()
        {
            foreach (int i in analogueInputs.Keys)
            {
                Thread t = new Thread(() => ThreadWork(analogueInputs[i]));
                tagThreads.Add(i, t);
            }
        }
        private static void AddDigitalInputs()
        {
            foreach (int i in digitalInputs.Keys)
            {
                Thread t = new Thread(() => ThreadWork(digitalInputs[i]));
                tagThreads.Add(i, t);
            }
        }
        private static void Start()
        {
            foreach (Thread t in tagThreads.Values)
            {
                t.Start();
            }
        }
        public static void ThreadWork(Input tag)
        {
            while (true)
            {
                if (tag.OnOffScan)
                {
                    double newValue = tag.Read();
                    OnTagValueChanged(tag, newValue);
                    UpdateTagXmlConfig();
                }
                Thread.Sleep(tag.ScanTime);
            }
        }

        private static void UpdateTagValueAtRuntime(Input tag, double newValue)
        {
            
            if (tag.GetType() == typeof(AnalogueInput))
            {
                UpdateAnalogueTagValue(tag, newValue);
            }
            else if (tag.GetType() == typeof(DigitalInput))
            {
                UpdateDigitalTagValue(tag, newValue);
            }
            
        }
        private static void UpdateDigitalTagValue(Input tag, double newValue)
        {
            Output o;
            double oldValue;
            DigitalInput di = (DigitalInput)tag;
            lock (digitalOutputTagsLock)
            {
                oldValue = digitalOutputs[di.TagName].Value;
                digitalOutputs[di.TagName].Value = newValue;
                o = digitalOutputs[di.TagName];
            }
            TrendingAppService.SendToTrendingApp(o, oldValue);
            ContextDataHandler.AddObservedTagValue(di, newValue, di.TagName);
        }
        private static void UpdateAnalogueTagValue(Input tag, double newValue)
        {
            Output o;
            double oldValue;
            AnalogueInput ai = (AnalogueInput)tag;
            lock (analogueOutputTagsLock)
            {
                oldValue = analogueOutputs[ai.TagName].Value;
                analogueOutputs[ai.TagName].Value = newValue;
                o = analogueOutputs[ai.TagName];
            }
            TrendingAppService.SendToTrendingApp(o, oldValue);
            ContextDataHandler.AddObservedTagValue(ai, newValue, ai.TagName);
        }

        public static void UpdateTagXmlConfig()
        {
            XmlDataIO.WriteTagsToXmlConfig();
        }
        

        private static void HandleAlarms(Input tag, double newValue)
        {
            if (tag.GetType() == typeof(AnalogueInput))
            {
                AnalogueInput ai = (AnalogueInput)tag;
                lock (alarmsLock)
                {
                    if (!alarms.ContainsKey(ai.TagName)) { return; }
                    foreach (Alarm a in alarms[ai.TagName])
                    {
                        if (a.IsAlarming(newValue))
                        {
                            a.GenerateMessage(ai.Units);
                            a.LogAlarm();
                            AlarmDisplayService.SendAlarms(a.Message, a.Priority);
                            ContextDataHandler.AddAlarmEvent(a);
                        }
                    }
                }
            }
        }


    }
}