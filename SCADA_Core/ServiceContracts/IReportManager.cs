﻿using SCADA_Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SCADA_Core.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IReportManagerService" in both code and config file together.
    [ServiceContract]
    public interface IReportManager
    {
        [OperationContract]
        List<AlarmEvent> GetAlarmsFromInterval(DateTime from, DateTime to);
        [OperationContract]
        List<AlarmEvent> GetAlarmsWithPriority(int priority);
        [OperationContract]
        List<ObservedTagData> TagValuesFromInterval(DateTime from, DateTime to);
        [OperationContract]
        List<ObservedTagData> NewestAITagValues();
        [OperationContract]
        List<ObservedTagData> NewestDITagValues();
        [OperationContract]
        List<ObservedTagData> AllTagValues(int tagName);
        [OperationContract]
        List<int> GetTags();
    }
}
