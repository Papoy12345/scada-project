﻿using SCADA_Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;

namespace SCADA_Core.ServiceContracts
{
    [ServiceContract(CallbackContract = typeof(IDatabaseManagerCallback), SessionMode = SessionMode.Required)]
    public interface IDatabaseManager
    {
        [OperationContract(IsInitiating = true)]
        void Login(string username, string password);
        [OperationContract]
        bool Register(string username, string password);
        [OperationContract]
        void AddTag(Input obj);
        [OperationContract]
        void RemoveTag(int name);
        [OperationContract]
        void SwitchScanOnOff(int tagName, bool onoff);
        [OperationContract]
        string GetOutputValue(int tagName);
        [OperationContract]
        void ChangeOutputValue(int tagName, double newValue);
        [OperationContract(IsTerminating = true)]
        void Logout();
        [OperationContract]
        void CreateAlarm(Alarm a);
        [OperationContract]
        bool RemoveAlarm(int name, int threshold);
        // added functions
        [OperationContract]
        Dictionary<string, List<int>> GetTagNames();

    }

    public interface IDatabaseManagerCallback
    {
        [OperationContract(IsOneWay = true)]
        void LoginCallback(bool isLoggedIn, bool isAdmin);

        [OperationContract(IsOneWay = true)]
        void TagNameExistsCallback();
    }
}