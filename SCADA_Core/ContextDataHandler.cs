﻿using SCADA_Core.Model;
using SCADA_Core.Model.IOTags;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SCADA_Core
{
    public class ContextDataHandler
    {
        public static readonly object TCLock = new object();
        public static readonly object UCLock = new object();
        public static readonly object ACLock = new object();
        public static TagDataContext TC;
        public static UserContext UC;
        public static AlarmEventContext AC;
        protected static void InitializeUserDb()
        {
            UC = new UserContext();
            if (!UC.Database.Exists())
            {
                User u = new User {
                    Username = "admin",
                    Password = User.EncryptData("admin"),
                    Admin = true
                };
                UC.Users.Add(u);
                UC.SaveChanges();
            }
            UC.Dispose();
        }

        public static void AddObservedTagValue(Input tag, double newValue, int tagName)
        {
            lock (TCLock)
            {
                TC = new TagDataContext();
                ObservedTagData otd = new ObservedTagData
                {
                    TagName = tagName,
                    Value = newValue,
                    TimeStamp = DateTime.Now,
                    TagType = tag.GetType().ToString()
                };
                try
                {
                    DefineIdForTagValue(otd);
                } catch (ArgumentNullException)
                {
                    otd.Id = 1;
                }
                
                TC.TagData.Add(otd);
                TC.SaveChanges();
                TC.Dispose();
            }
        }
        
        public static void AddAlarmEvent(Alarm a)
        {
            lock (ACLock)
            {
                AC = new AlarmEventContext();
                AlarmEvent ae = new AlarmEvent
                {
                    Message = a.Message,
                    TimeStamp = a.TimeStamp,
                    Priority = a.Priority
                };
                try
                {
                    DefineIdForEvent(ae);
                } catch (ArgumentNullException)
                {
                    ae.Id = 1;
                }
                
                AC.Alarms.Add(ae);
                AC.SaveChanges();
                AC.Dispose();
            }
        }

        private static void DefineIdForEvent(AlarmEvent ae)
        {
            int id = 1;
            if (!AC.Database.Exists())
            {
                ae.Id = id;
            }
            else
            {
                foreach (AlarmEvent dba in AC.Alarms)
                {
                    id++;
                }
                ae.Id = id;
            }
        }
        private static void DefineIdForTagValue(ObservedTagData otd)
        {
            int id = 1;
            if (!TC.Database.Exists())
            {
                otd.Id = id;
            } else 
            {
                foreach (ObservedTagData o in TC.TagData)
                {
                    id++;
                }
                otd.Id = id;
            }
        }
    }
}