﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SCADA_Core.Model
{
    public class ObservedTagData
    {
        [Key]
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public double Value { get; set; }
        public string TagType { get; set; }
        public int TagName { get; set; }
    }
}