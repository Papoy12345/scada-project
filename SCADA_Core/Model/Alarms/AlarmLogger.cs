﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace SCADA_Core.Model
{
    public class AlarmLogger
    {
        public void AppendLog(string message)
        {
            string path = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data", "alarmsLog.txt");
            if (!File.Exists(path))
            {
                StreamWriter s = File.CreateText(path);
                s.Close();
            }
            StreamWriter sw = File.AppendText(path);
            sw.WriteLine(message);
            sw.Flush();
            sw.Close();
        }
    }
}