﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SCADA_Core.Model
{
    public class AlarmEvent
    {
        [Key]
        public int Id { get; set; }
        public string Message { get; set; }
        public int Priority { get; set; }
        public DateTime TimeStamp { get; set; }
    }

    public class AlarmEventContext : DbContext
    {
        public DbSet<AlarmEvent> Alarms { get; set; }

    }
}