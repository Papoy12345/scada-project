﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SCADA_Core.Model
{
    [DataContract]
    [KnownType(typeof(AnalogueInput))]
    [KnownType(typeof(DigitalInput))]
    public abstract class Input
    {
        [DataMember]
        public bool OnOffScan { get; set; }
        [DataMember]
        public int ScanTime { get; set; }

        public abstract double Read();
    }
}