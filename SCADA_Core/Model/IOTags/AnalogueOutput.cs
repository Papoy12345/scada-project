﻿using SCADA_Core.Interfaces;
using SCADA_Core.Model.IOTags;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SCADA_Core.Model
{
    public class AnalogueOutput : Output, ITag, IAnalogue
    {
        public int TagName { get; set; }
        public string Description { get; set; }
        public char IOAdress { get; set; }
        public int LowLimit { get; set; }
        public int HighLimit { get; set; }
        public string Units { get; set; }

        public override string ToStringWhenValueChanged(double oldValue)
        {
            return $"Analogue Tag {TagName} changed value from {oldValue} to {Value}.";
        }
    }
}