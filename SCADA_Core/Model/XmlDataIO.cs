﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using System.Xml.Linq;

namespace SCADA_Core.Model
{
    public class XmlDataIO
    {
        private static readonly object WriteLock = new object();
        public static void WriteTagsToXmlConfig()
        {
            XElement aInputs = new XElement("AnalogueInputs", from i in TagProcessor.analogueInputs.Values
                                                                select new XElement("AnalogueInput",
                                                                new XAttribute("TagName", i.TagName),
                                                                new XAttribute("OnOffScan", i.OnOffScan),
                                                                new XAttribute("Description", i.Description),
                                                                new XAttribute("HighLimit", i.HighLimit),
                                                                new XAttribute("LowLimit", i.LowLimit),
                                                                new XAttribute("Units", i.Units),
                                                                new XAttribute("ScanTime", i.ScanTime),
                                                                new XAttribute("IOAdress", i.IOAdress)));
            XElement dInputs = new XElement("DigitalInputs", from j in TagProcessor.digitalInputs.Values
                                                                select new XElement("DigitalInput",
                                                                new XAttribute("TagName", j.TagName),
                                                                new XAttribute("OnOffScan", j.OnOffScan),
                                                                new XAttribute("Description", j.Description),
                                                                new XAttribute("ScanTime", j.ScanTime),
                                                                new XAttribute("IOAdress", j.IOAdress)));
            XElement aOutputs = new XElement("AnalogueOutputs", from k in TagProcessor.analogueOutputs.Values
                                                                select new XElement("AnalogueOutput",
                                                                    new XAttribute("TagName", k.TagName),
                                                                    new XAttribute("Description", k.Description),
                                                                    new XAttribute("IOAdress", k.IOAdress),
                                                                    new XAttribute("LowLimit", k.LowLimit),
                                                                    new XAttribute("HighLimit", k.HighLimit),
                                                                    new XAttribute("Units", k.Units),
                                                                    new XAttribute("InitialValue", k.InitialValue),
                                                                    new XAttribute("Value", k.Value)));
            XElement dOutputs = new XElement("DigitalOutputs", from l in TagProcessor.digitalOutputs.Values
                                                                select new XElement("DigitalOutput",
                                                                    new XAttribute("TagName", l.TagName),
                                                                    new XAttribute("Description", l.Description),
                                                                    new XAttribute("IOAdress", l.IOAdress),
                                                                    new XAttribute("InitialValue", l.InitialValue),
                                                                    new XAttribute("Value", l.Value)));
            List<XElement> ie = new List<XElement>
            {
                aInputs,
                dInputs,
                aOutputs,
                dOutputs
            };
            XElement v = new XElement("Data", ie);
            XDocument doc = new XDocument(v);
            doc.Declaration = new XDeclaration("1.0", "utf-8", "true");
            string TagConfigXml = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data", "tagConfig.xml");
            lock (WriteLock)
            {
                doc.Save(TagConfigXml);
            }
           
            
        }
        public static void ReadTagXmlConfig()
        {
            string TagConfigXml = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data", "tagConfig.xml");
            XElement data = XElement.Load(TagConfigXml);
            IEnumerable<XElement> analogueInputs    = data.Descendants("AnalogueInput") ;
            IEnumerable<XElement> analogueOutputs   = data.Descendants("AnalogueOutput");
            IEnumerable<XElement> digitalInputs     = data.Descendants("DigitalInput")  ;
            IEnumerable<XElement> digitalOutputs    = data.Descendants("DigitalOutput") ;
            SetAnalogueInputs(analogueInputs)   ;
            SetAnalogueOutputs(analogueOutputs) ;
            SetDigitalInputs(digitalInputs)     ;
            SetDigitalOutputs(digitalOutputs)   ;
        }
        private static void SetAnalogueInputs(IEnumerable<XElement> analogueInputs)
        {
            foreach (XElement xe in analogueInputs)
            {
                AnalogueInput ai = new AnalogueInput
                {
                    TagName     = int.Parse(xe.Attribute("TagName")     .Value),
                    OnOffScan   = bool.Parse(xe.Attribute("OnOffScan")  .Value),
                    Description = xe.Attribute("Description")           .Value,
                    HighLimit   = int.Parse(xe.Attribute("HighLimit")   .Value),
                    LowLimit    = int.Parse(xe.Attribute("LowLimit")    .Value),
                    Units       = xe.Attribute("Units")                 .Value,
                    ScanTime    = int.Parse(xe.Attribute("ScanTime")    .Value),
                    IOAdress    = char.Parse(xe.Attribute("IOAdress")   .Value)
                };
                TagProcessor.analogueInputs.Add(ai.TagName, ai);
            }
        }
        private static void SetAnalogueOutputs(IEnumerable<XElement> analogueOutputs)
        {
            foreach (XElement xe in analogueOutputs)
            {
                AnalogueOutput ao = new AnalogueOutput
                {
                    TagName         = int.Parse(xe.Attribute("TagName")         .Value),
                    Description     = xe.Attribute("Description")               .Value,
                    HighLimit       = int.Parse(xe.Attribute("HighLimit")       .Value),
                    LowLimit        = int.Parse(xe.Attribute("LowLimit")        .Value),
                    Units           = xe.Attribute("Units")                     .Value,
                    IOAdress        = char.Parse(xe.Attribute("IOAdress")       .Value),
                    InitialValue    = double.Parse(xe.Attribute("InitialValue") .Value),
                    Value           = double.Parse(xe.Attribute("Value")        .Value)
                };
                TagProcessor.analogueOutputs.Add(ao.TagName, ao);
            }
        }
        private static void SetDigitalInputs(IEnumerable<XElement> digitalInputs)
        {
            foreach (XElement xe in digitalInputs)
            {
                DigitalInput di = new DigitalInput
                {
                    TagName     = int.Parse(xe.Attribute("TagName")     .Value),
                    OnOffScan   = bool.Parse(xe.Attribute("OnOffScan")  .Value),
                    Description = xe.Attribute("Description")           .Value,
                    ScanTime    = int.Parse(xe.Attribute("ScanTime")    .Value),
                    IOAdress    = char.Parse(xe.Attribute("IOAdress")   .Value),
                };
                TagProcessor.digitalInputs.Add(di.TagName, di);
            }
        }
        private static void SetDigitalOutputs(IEnumerable<XElement> digitalOutputs)
        {
            foreach (XElement xe in digitalOutputs)
            {
                DigitalOutput dou = new DigitalOutput
                {
                    TagName         = int.Parse(xe.Attribute("TagName")         .Value),
                    Description     = xe.Attribute("Description")               .Value,
                    IOAdress        = char.Parse(xe.Attribute("IOAdress")       .Value),
                    InitialValue    = double.Parse(xe.Attribute("InitialValue") .Value),
                    Value           = double.Parse(xe.Attribute("Value")        .Value)
                };
                TagProcessor.digitalOutputs.Add(dou.TagName, dou);
            }
        }
    

        public static void WriteAlarmsToXmlConfig()
        {
            List<XElement> ie = new List<XElement>();
            XElement alarms = new XElement("Alarms");
            foreach (List<Alarm> lAlarms in TagProcessor.alarms.Values)
            {
                alarms.Add(from i in lAlarms
                           select new XElement("Alarm",
                           new XAttribute("Name", i.Name),
                           new XAttribute("Priority", i.Priority),
                           new XAttribute("Type", i.Type),
                           new XAttribute("Threshold", i.Threshold)));
                ie.Add(alarms);
            }
            XDocument doc = new XDocument(alarms);
            doc.Declaration = new XDeclaration("1.0", "utf-8", "true");
            string alarmsConfigXml = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data", "alarmsConfig.xml");
            doc.Save(alarmsConfigXml);
        }
        public static void ReadAlarmXmlConfig()
        {
            string AlarmConfigXml = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data", "alarmsConfig.xml");
            XElement data = XElement.Load(AlarmConfigXml);
            IEnumerable<XElement> alarms = data.Descendants("Alarm");
            SetAlarms(alarms);
        }
        private static void SetAlarms(IEnumerable<XElement> alarms)
        {
            TagProcessor.alarms = new Dictionary<int, List<Alarm>>();
            foreach (XElement xe in alarms)
            {
                int Name = int.Parse(xe.Attribute("Name").Value);
                if (!TagProcessor.alarms.ContainsKey(Name))
                {
                    TagProcessor.alarms.Add(Name, new List<Alarm>());
                }
                Alarm a = new Alarm
                {
                    Name = Name,
                    Priority = int.Parse(xe.Attribute("Priority").Value),
                    Type = (Alarm.AlarmType)Enum.Parse(typeof(Alarm.AlarmType), xe.Attribute("Type").Value),
                    Threshold = int.Parse(xe.Attribute("Threshold").Value),
                };
                TagProcessor.alarms[Name].Add(a);
            }
        }
    }
}