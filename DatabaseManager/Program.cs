﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using SCADA_Core.Model;
using System.Threading.Tasks;

namespace DatabaseManager
{
    public class ManagerCallback : ServiceReference.IDatabaseManagerCallback
    {

        public bool IsLoggedIn { get; set; }
        public bool IsAdmin = false;

        public void LoginCallback(bool isLoggedIn, bool isAdmin)
        {
            IsLoggedIn = isLoggedIn;
            IsAdmin = isAdmin;
            if (!IsLoggedIn)
            {
                Console.WriteLine("Login failed.");
            }
        }

        public void TagNameExistsCallback()
        {
            Console.WriteLine("Tag with inputted tag name already exists.");
        }
    }

    public class Program
    {
        static void Main(string[] args)
        {
            ManagerCallback mcb = new ManagerCallback();
            InstanceContext ic = new InstanceContext(mcb);
            ServiceReference.DatabaseManagerClient dmc = new ServiceReference.DatabaseManagerClient(ic);
            StartMenu(dmc, mcb);


        }

        public static void Login(ServiceReference.DatabaseManagerClient dmc, ManagerCallback mcb)
        {
            string username = "Username: ";
            string password = "Password: ";
            Console.Write(username);
            username = Console.ReadLine();
            Console.Write(password);
            password = Console.ReadLine();

            dmc.Login(username, password);
            if (mcb.IsLoggedIn)
            {
                Menu(dmc, mcb.IsAdmin);
            }
            return;
        }

        private static void StartMenu(ServiceReference.DatabaseManagerClient dmc, ManagerCallback mcb)
        {
            StringBuilder options = new StringBuilder();
            options.Append("1. Login\n");
            options.Append("2. Quit\n");
            while (true)
            {
                Console.Write(options.ToString());
                char op = Console.ReadKey(true).KeyChar;
                switch (op)
                {
                    case '1':
                        Login(dmc, mcb);
                        break;
                    case '2':
                        return;
                    default:
                        Console.WriteLine("Invalid input.");
                        continue;
                }
            }
        }
        private static void Menu(ServiceReference.DatabaseManagerClient dmc, bool isAdmin)
        {
            StringBuilder options = new StringBuilder();
            if (isAdmin) { options.Append("0. Register user\n"); }
            options.Append("1. Change output value\n");
            options.Append("2. Get output value\n");
            options.Append("3. Turn scan on/off\n");
            options.Append("4. Add tag\n");
            options.Append("5. Remove tag\n");
            options.Append("6. Create alarm\n");
            options.Append("7. Delete alarm\n");
            options.Append("8. Log out\n");
            while (true)
            {
                Console.Write(options.ToString());
                char op = Console.ReadKey(true).KeyChar;
                try
                {
                    switch (op)
                    {
                        case '0':
                            if (!isAdmin) { continue; }
                            string username = ManagerOptionMenu.InputRegistrationCredentials("Input username: ");
                            string password = ManagerOptionMenu.InputRegistrationCredentials("Input password: ");
                            dmc.Register(username, password);
                            break;
                        case '1':
                            int tagName1 = ManagerOptionMenu.PickTag(dmc.GetTagNames());
                            double newValue = ManagerOptionMenu.EnterNewValue();
                            dmc.ChangeOutputValue(tagName1, newValue);
                            break;
                        case '2':
                            dmc.GetOutputValue(ManagerOptionMenu.PickTag(dmc.GetTagNames()));
                            break;
                        case '3':
                            int tagName3 = ManagerOptionMenu.PickTag(dmc.GetTagNames());
                            bool onoff = ManagerOptionMenu.SwitchScanOnOff();
                            dmc.SwitchScanOnOff(tagName3, onoff);
                            break;
                        case '4':
                            Input o = ManagerOptionMenu.AddTag();
                            if (null != o) { dmc.AddTag(o); }
                            break;
                        case '5':
                            dmc.RemoveTag(ManagerOptionMenu.PickTag(dmc.GetTagNames()));
                            break;
                        case '6': 
                            dmc.CreateAlarm(ManagerOptionMenu.CreateAlarm(dmc.GetTagNames()));
                            break;
                        case '7':
                            ManagerOptionMenu.RemoveAlarm(dmc.GetTagNames(), dmc);
                            break;
                        case '8': dmc.Logout(); return;
                        default: continue;
                    }
                } catch (Exception)
                {
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("Invalid input.");
                    Console.ResetColor();
                    continue;
                }
                
            }
        }    
    }
}
